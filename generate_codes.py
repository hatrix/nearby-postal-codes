#!/usr/bin/env python3

from math import acos, radians, sin, cos

class PostalCodes():
    def __init__(self, path='./laposte_hexasmal.csv'):
        self.data = self.load_csv(path)

    def load_csv(self, path):
        f = open(path)
        lines = f.readlines()

        data = {}
        for line in lines[1:]:
            infos = line.split(';')
            code = infos[2]
            gps = [point.strip() for point in infos[5].split(',')]

            if len(gps) == 2 and code:
                data[code] = (gps[0], gps[1])

        return data

    def get_nearby(self, code, radius=10): # radius in km
        if code not in self.data.keys():
            return None

        lat_ini = float(self.data[code][0])
        long_ini = float(self.data[code][1])

        ok_codes = []
        for c, (latitude, longitude) in self.data.items():
            try:
                latitude = float(latitude)
                longitude = float(longitude)
            except:
                continue
    
            dist = acos(sin(radians(lat_ini)) * sin(radians(latitude))
                   + cos(radians(lat_ini)) * cos(radians(latitude))
                   * cos(radians(long_ini - longitude))) * 6366

            if dist <= radius:
                ok_codes.append((c, dist))

        return sorted(ok_codes, key=lambda x: x[1])


if __name__ == "__main__":
    pc = PostalCodes()
    nearby = pc.get_nearby('75116', radius=10)
    for code, dist in nearby:
        print('"{}",'.format(code))
